Drop table PropertyType cascade constraints;
Drop table BuildingType cascade constraints;
Drop table Room cascade constraints;
Drop table Floor cascade constraints;
Drop table Building cascade constraints; 
Drop table BuildingDes cascade constraints; 
Drop table Properties cascade constraints;
Drop table Land cascade constraints;
Drop table FarmDes cascade constraints; 
Drop table FarmType cascade constraints;
Drop table LiveStock cascade constraints;
Drop table Crop cascade constraints;

CREATE TABLE PropertyType( 
  Id  NUMBER(3) generated as IDENTITY primary key ,
  Name VARCHAR2(50) NOT NULL
);
insert into PropertyType(Name) values ('Residential');
insert into PropertyType(Name) values ('Commercial');
insert into PropertyType(Name) values ('Agriculture');

Create table BuildingType(
  Id Number(3) generated as IDENTITY primary key,
  name varchar2(50)
);

insert into BuildingType(Name) values('Detached home');
insert into BuildingType(Name) values('Semi-detached home');
insert into BuildingType(Name) values('Free-hold townhouse');
insert into BuildingType(Name) values('Condo townhouse');
insert into BuildingType(Name) values('Deparment condo');
insert into BuildingType(Name) values('Farm');
insert into BuildingType(Name) values('Office');
insert into BuildingType(Name) values('Business');

Create table Floor(
  Id Number(3)generated as IDENTITY primary key,
  Name varchar2(20)
);
insert into Floor(Name) values('Basement');
insert into Floor(Name) values('Main floor');
insert into Floor(Name) values('Second level');
insert into Floor(Name) values('third level');
insert into Floor(Name) values('fourth level');
insert into Floor(Name) values('fifth level');

Create table Room(
  Id Number(3)generated as IDENTITY primary key,
  Name varchar2(50)
);
insert into Room(Name) values('Bedroom');
insert into Room(Name) values('Living Room');
insert into Room(Name) values('Enclosed porch');
insert into Room(Name) values('Kitchen');
insert into Room(Name) values('Storage');
insert into Room(Name) values('bath room');
insert into Room(Name) values('Master bedroom');

Create table Building(
  Id Number(3)generated as IDENTITY primary key,
  BT_Id Number(3),
  HeatingType varchar2(50),
  HeatingFuel varchar2(50),
  Flooring varchar2(50),
  FoundationType varchar2(50),
  RoofStyle varchar2(50),
  RoofingMaterial varchar2(50),
  FloorSpace varchar2(50),
  ExteriorBuildingSize varchar2(50),
  Constraint Building_BT_Id foreign key (BT_Id) references BuildingType(Id)
);

create table BuildingDes(
  Id number(3)generated as IDENTITY primary key,
  BuildingId Number(3),
  floorId Number(3),
  roomId Number(3),
  Demension varchar2(50),
  count Number(3),
  constraint BuildingDes_BuildingId FOREIGN key (BuildingId) references Building(Id),
  constraint BuildingDes_floorId foreign key (floorId) references Floor(Id),
  constraint Buildingdes_roomId foreign key (roomId) references Room(Id)
);

create table Land(
  Id number(3)generated as IDENTITY primary key,
  SoilType varchar2(50),
  SurfaceWater varchar2(50),
  Fencing varchar2(50)
);

create table FarmType(
  Id Number(3)generated as IDENTITY primary key,
  Name varchar2(50)
);

insert into FarmType(Name) values('Animal');
insert into FarmType(Name) values('Cash Crop');
insert into FarmType(Name) values('Orchard');

create table LiveStock(
  Id Number(3)generated as IDENTITY primary key,
  Name varchar2(50)
);

insert into LiveStock(Name) values('Horse');
insert into LiveStock(Name) values('Beef');
insert into LiveStock(Name) values('Hog');

create table Crop(
  Id Number(3)generated as IDENTITY primary key,
  Name varchar2(50)
);

insert into Crop(Name) values('Beans');
insert into Crop(Name) values('Corn');
insert into Crop(Name) values('Rice');
insert into Crop(Name) values('Fruit');

Create table Properties(
  Id Number(3) generated as IDENTITY primary key,
  PT_Id Number(3),
  BuildingId Number(3),
  LandId Number(3),
  sale_rent NUMBER(1), /* 1:sale 0:rent*/
  title varchar2(100) not null,
  Address varchar2(50) not null,
  LandSize varchar2(50),
  Price varchar2(50),
  Built_in varchar2(4),
  Storeys number(3),
  ParkingType varchar2(50),
  scene varchar2(50),
  features varchar2(50),
  AppliancesIncluded varchar2(100),
  AmenitiesNearby varchar2(100),
  Description varchar2(200),
  Constraint Properties_PT_Id foreign key (PT_Id) references PropertyType(id),
  constraint Properties_BuildingId FOREIGN key (BuildingId) references Building(Id),
  constraint Properties_LandId FOREIGN key (LandId) references Land(Id)
);

create table FarmDes(
  Id number(3)generated as IDENTITY primary key,
  PropertyId Number(3),
  FarmTypeId Number(3),
  LiveStockId Number(3),
  CropId Number(3),
  Constraint FarmDes_PropertyId foreign key (PropertyId) references Properties(Id),
  constraint FarmDes_FarmTypeId FOREIGN key (FarmTypeId) references FarmType(Id),
  constraint FarmDes_LiveStockId FOREIGN key (LiveStockId) references LiveStock(Id),
  constraint FarmDes_CropId FOREIGN key (CropId) references Crop(Id)
);




