/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.BuildingDes;
import Service.BaseTypeService;
import Service.BuildingService;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;

/**
 *
 * @author linh
 */
public class BuildingDesDao extends BaseDao<BuildingDes>{
    public BuildingDesDao(){
        tableName = Constants.BD_TB;
    }
    
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, Vector<BuildingDes> buildingDeses)
    { 
        BuildingService buildingService = new BuildingService();
        BaseTypeService floorService = new BaseTypeService(Constants.FLOOR_TB);
        BaseTypeService roomService = new BaseTypeService(Constants.ROOM_TB);      
        buildingService.getConnection();
        floorService.getConnection();
        roomService.getConnection();
        try {
            while(data.next())
            {
                BuildingDes buildingDes=new BuildingDes();
                buildingDes.setId(data.getInt(Constants.ID));
                buildingDes.setBuilding(buildingService.getById(data.getInt(Constants.BD_BUILDINGID)));
                buildingDes.setFloor(floorService.getById(data.getInt(Constants.BD_FLOORID)));
                buildingDes.setRoom(roomService.getById(data.getInt(Constants.BD_ROOMID)));
                buildingDes.setDemesion(data.getString(Constants.BD_DEMESION));
                buildingDes.setCount(data.getInt(Constants.BD_COUNT));
                buildingDeses.add(buildingDes);
            }
            
            floorService.closeConnection();
            roomService.closeConnection();
            buildingService.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(BuildingDesDao.class.getName()).log(Level.SEVERE, null, ex);
            floorService.closeConnection();
            roomService.closeConnection();
            buildingService.closeConnection();
        }
    }
    
    @SuppressWarnings("unchecked")
    public java.util.Vector convertToTbData(ResultSet data, int colCount)
    {   java.util.Vector<BuildingDes> buildingDeses = new java.util.Vector<>();
        this.convertToDomain(data, buildingDeses);
        java.util.Vector buildingDesData = new java.util.Vector<>();
        for (int i = 0; i < buildingDeses.size(); i++) {
            java.util.Vector record = new java.util.Vector();
            record.add(buildingDeses.get(i).getId());
            record.add(buildingDeses.get(i).getBuilding().getId());
            record.add(buildingDeses.get(i).getFloor().getName());
            record.add(buildingDeses.get(i).getRoom().getName());
            record.add(buildingDeses.get(i).getDemesion());
            record.add(buildingDeses.get(i).getCount());
            buildingDesData.add(record);
        }
        return buildingDesData;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(BuildingDes buildingDes,java.util.Vector<Criterion> data)
    {
        Object id =buildingDes.getId();
        data.add(new Criterion(Constants.BD_BUILDINGID,buildingDes.getBuilding().getId()+""));
        data.add(new Criterion(Constants.BD_FLOORID,buildingDes.getFloor().getId()+""));
        data.add(new Criterion(Constants.BD_ROOMID,buildingDes.getRoom().getId()+""));
        data.add(new Criterion(Constants.BD_DEMESION,buildingDes.getDemesion()));
        data.add(new Criterion(Constants.BD_COUNT,buildingDes.getCount()+""));
     
        return id;
    }
    
}
