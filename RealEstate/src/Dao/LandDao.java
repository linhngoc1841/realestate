/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.Land;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author linh
 */
public class LandDao extends BaseDao<Land>{
    public LandDao(){
        tableName = Constants.LAND_TB;
    }
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, java.util.Vector<Land> lands)
    {
        try {
            while(data.next())
            {
                Land land=new Land();
                land.setId(data.getInt(Constants.ID));
                land.setSoilType(data.getString(Constants.LAND_SOILTYPE));
                land.setSurfaceWater(data.getString(Constants.LAND_SURFACEWATER));
                land.setFencing(data.getString(Constants.LAND_FENCING));
                lands.add(land);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LandDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    public java.util.Vector convertToTbData(ResultSet data, int colCount)
    {
        java.util.Vector lands = new java.util.Vector<>();
        try {
            while(data.next())
            {
                java.util.Vector record = new java.util.Vector();
                for (int i = 0; i < colCount; i++) {
                    record.add(data.getString(i + 1));
                }
                lands.add(record);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LandDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lands;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(Land land,java.util.Vector<Criterion> data)
    {
        Object id =land.getId();
        data.add(new Criterion(Constants.LAND_SOILTYPE, land.getSoilType()));
        data.add(new Criterion(Constants.LAND_SURFACEWATER, land.getSurfaceWater()));
        data.add(new Criterion(Constants.LAND_FENCING, land.getFencing()));
        return id;
    }
    
}
