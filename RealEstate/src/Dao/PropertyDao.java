/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.Properties;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;

/**
 *
 * @author linh
 */
public class PropertyDao extends BaseDao<Properties>{
    
    public PropertyDao(){
        tableName = Constants.PRO_TB;
    }
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, java.util.Vector<Properties> properties)
    {   BaseTypeDao baseTypeDao  = new BaseTypeDao(Constants.PT_TB);
        BuildingDao buildingDao  = new BuildingDao();
        LandDao landDao  = new LandDao();
        baseTypeDao.getConnection();
        buildingDao.getConnection();
        landDao.getConnection();
        try {
            while(data.next())
            {
                Properties property=new Properties();
                property.setId(data.getInt(Constants.ID));
                property.setPropertyType(baseTypeDao.getById(data.getInt(Constants.PRO_PT_ID)));
                property.setBuilding(buildingDao.getById(data.getInt(Constants.PRO_BUILDINGID)));
                property.setLand(landDao.getById(data.getInt(Constants.PRO_LANDID)));
                property.setSaleOrrent(data.getInt(Constants.PRO_SALERENT));
                property.setTitle(data.getString(Constants.PRO_TITLE));
                property.setAddress(data.getString(Constants.PRO_ADDRESS));
                property.setLandSize(data.getString(Constants.PRO_LANDSIZE));
                property.setPrice(data.getDouble(Constants.PRO_PRICE));
                property.setBuiltIn(data.getString(Constants.PRO_BUILT_IN));
                property.setStoreys(data.getInt(Constants.PRO_STOREYS));
                property.setParkingType(data.getString(Constants.PRO_PARKINGTYPE));
                property.setScene(data.getString(Constants.PRO_SCENE));
                property.setFeatures(data.getString(Constants.PRO_FEATURES));
                property.setAmenitiesNearby(data.getString(Constants.PRO_AMENITIES_NEARBY));
                property.setAppliancesIncluded(data.getString(Constants.PRO_APP_INCLUDED));
                property.setDescription(data.getString(Constants.PRO_DES));
                
                properties.add(property);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PropertyDao.class.getName()).log(Level.SEVERE, null, ex);
            baseTypeDao.closeConnection();
            buildingDao.closeConnection();
            landDao.closeConnection();
        }
        baseTypeDao.closeConnection();
        buildingDao.closeConnection();
        landDao.closeConnection();
    }
    
    @SuppressWarnings("unchecked")
    public java.util.Vector convertToTbData(ResultSet data, int colCount)
    {
        Vector properties = new Vector<>();
        try {
            while(data.next())
            {
                java.util.Vector record = new java.util.Vector();
                for (int i = 0; i < colCount; i++) {
                    record.add(data.getString(i + 1));
                }
                properties.add(record);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PropertyDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return properties;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(Properties property,java.util.Vector<Criterion> data)
    {
        Object id =property.getId();
        data.add(new Criterion(Constants.PRO_PT_ID,property.getPropertyType().getId()+""));
        if(property.getBuilding()!=null)
            data.add(new Criterion(Constants.PRO_BUILDINGID,property.getBuilding().getId()+""));
        else
            data.add(new Criterion(Constants.PRO_BUILDINGID,null));
        if(property.getLand() != null)
            data.add(new Criterion(Constants.PRO_LANDID,property.getLand().getId()+""));
        else
            data.add(new Criterion(Constants.PRO_LANDID,null));
        data.add(new Criterion(Constants.PRO_SALERENT,property.getSaleOrrent()+""));
        data.add(new Criterion(Constants.PRO_TITLE,property.getTitle()));
        data.add(new Criterion(Constants.PRO_ADDRESS,property.getAddress()));
        data.add(new Criterion(Constants.PRO_LANDSIZE,property.getLandSize()));
        data.add(new Criterion(Constants.PRO_PRICE,property.getPrice()+""));
        data.add(new Criterion(Constants.PRO_BUILT_IN,property.getBuiltIn()));
        data.add(new Criterion(Constants.PRO_STOREYS,property.getStoreys()+""));
        data.add(new Criterion(Constants.PRO_PARKINGTYPE,property.getParkingType()));
        data.add(new Criterion(Constants.PRO_SCENE,property.getScene()));
        data.add(new Criterion(Constants.PRO_FEATURES,property.getFeatures()));
        data.add(new Criterion(Constants.PRO_AMENITIES_NEARBY,property.getAmenitiesNearby()));
        data.add(new Criterion(Constants.PRO_APP_INCLUDED,property.getAppliancesIncluded()));
        data.add(new Criterion(Constants.PRO_DES,property.getDescription()));
        return id;
    }
}
