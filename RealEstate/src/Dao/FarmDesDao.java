/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.FarmDes;
import Service.BaseTypeService;
import Service.PropertyService;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author linh
 */
public class FarmDesDao extends BaseDao<FarmDes>{
    public FarmDesDao(){
        tableName = Constants.FD_TB;
    }
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, Vector<FarmDes> farmDeses)
    {   PropertyService propertyService = new PropertyService();
        BaseTypeService farmTypeService = new BaseTypeService(Constants.FT_TB);
        BaseTypeService cropService = new BaseTypeService(Constants.CROP_TB);
        BaseTypeService liveStockService = new BaseTypeService(Constants.LS_TB);
        propertyService.getConnection();
        farmTypeService.getConnection();
        cropService.getConnection();
        liveStockService.getConnection();
        try {
            while(data.next())
            {
                FarmDes farmDes=new FarmDes();
                farmDes.setId(data.getInt(Constants.ID));
                farmDes.setProperties(propertyService.getById(data.getInt(Constants.FD_PROPERTYID)));
                farmDes.setFarmType(farmTypeService.getById(data.getInt(Constants.FD_FRAMTYPEID)));
                farmDes.setCrop(cropService.getById(data.getInt(Constants.FD_CROPID)));
                farmDes.setLiveStock(liveStockService.getById(data.getInt(Constants.FD_LIVESTOCKID)));
                farmDeses.add(farmDes);
            }
            
            farmTypeService.closeConnection();
            cropService.closeConnection();
            liveStockService.closeConnection();
            propertyService.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(FarmDesDao.class.getName()).log(Level.SEVERE, null, ex);
            farmTypeService.closeConnection();
            cropService.closeConnection();
            liveStockService.closeConnection();
            propertyService.closeConnection();
        }
    }
    
    @SuppressWarnings("unchecked")
    public Vector convertToTbData(ResultSet data, int colCount)
    {   Vector<FarmDes> farmDeses = new Vector<>();
        this.convertToDomain(data, farmDeses);
        Vector farmDesData = new Vector<>();
        for (int i = 0; i < farmDeses.size(); i++) {
            Vector record = new Vector();
            record.add(farmDeses.get(i).getId());
            record.add(farmDeses.get(i).getProperties().getId());
            record.add(farmDeses.get(i).getFarmType().getName());
            if(farmDeses.get(i).getLiveStock() != null)
                record.add(farmDeses.get(i).getLiveStock().getName());
            else
                record.add("");
            if(farmDeses.get(i).getCrop()!=null)
                record.add(farmDeses.get(i).getCrop().getName());
            else
                record.add("");
            farmDesData.add(record);
        }
        return farmDesData;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(FarmDes farmDes,java.util.Vector<Criterion> data)
    {
        Object id =farmDes.getId();
        data.add(new Criterion(Constants.FD_PROPERTYID,farmDes.getProperties().getId()+""));
        if(farmDes.getFarmType()!=null)
            data.add(new Criterion(Constants.FD_FRAMTYPEID,farmDes.getFarmType().getId()+""));
        else
            data.add(new Criterion(Constants.FD_FRAMTYPEID,null));
        if(farmDes.getCrop()!=null)
            data.add(new Criterion(Constants.FD_CROPID,farmDes.getCrop().getId()+""));
        else
            data.add(new Criterion(Constants.FD_CROPID,null));
        if(farmDes.getLiveStock()!=null)
            data.add(new Criterion(Constants.FD_LIVESTOCKID,farmDes.getLiveStock().getId()+""));
        else
            data.add(new Criterion(Constants.FD_LIVESTOCKID,null));
     
        return id;
    }
    
}
