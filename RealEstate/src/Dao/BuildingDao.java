/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.Building;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author linh
 */
public class BuildingDao extends BaseDao<Building>{
    public BuildingDao(){
        tableName = Constants.BUILDING_TB;
    }
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, java.util.Vector<Building> buildings)
    {
        BaseTypeDao baseTypeDao = new BaseTypeDao(Constants.BT_TB);
        baseTypeDao.getConnection();
  
        try {
            while(data.next())
            {
                Building building =new Building();
                building.setId(data.getInt(Constants.ID));
                building.setBuildingType(baseTypeDao.getById(data.getInt(Constants.BUILDING_BT_ID)));
                building.setHeatingType(data.getString(Constants.BUILDING_HEATINGTYPE));
                building.setHeatingFuel(data.getString(Constants.BUILDING_HEATINGFUEL));
                building.setFlooring(data.getString(Constants.BUILDING_FLOORING));
                building.setFoundationType(data.getString(Constants.BUILDING_FOUNDATIONTYPE));
                building.setRoofStyle(data.getString(Constants.BUILDING_ROOFSTYLE));
                building.setRoofingMaterial(data.getString(Constants.BUILDING_ROOFINGMATERIAL));
                building.setFloorSpace(data.getString(Constants.BUILDING_FLOORSPACE));
                building.setExteriorBuildingSize(data.getString(Constants.BUILDING_EXTERIORBUILDINGSIZE));
                buildings.add(building);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BuildingDao.class.getName()).log(Level.SEVERE, null, ex);
            baseTypeDao.closeConnection();
        }
        baseTypeDao.closeConnection();
    }
    
    @SuppressWarnings("unchecked")
    public java.util.Vector convertToTbData(ResultSet data, int colCount)
    {
        java.util.Vector buildings = new java.util.Vector<>();
        try {
            while(data.next())
            {
                java.util.Vector record = new java.util.Vector();
                for (int i = 0; i < colCount; i++) {
                    record.add(data.getString(i + 1));
                }
                buildings.add(record);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BuildingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return buildings;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(Building building,java.util.Vector<Criterion> data)
    {
        Object id =building.getId();
        data.add(new Criterion(Constants.BUILDING_BT_ID, building.getBuildingType().getId() + ""));
        data.add(new Criterion(Constants.BUILDING_HEATINGTYPE, building.getHeatingType()));
        data.add(new Criterion(Constants.BUILDING_HEATINGFUEL, building.getHeatingFuel()));
        data.add(new Criterion(Constants.BUILDING_FLOORING, building.getFlooring()));
        data.add(new Criterion(Constants.BUILDING_FOUNDATIONTYPE, building.getFoundationType()));
        data.add(new Criterion(Constants.BUILDING_ROOFSTYLE, building.getRoofStyle()));
        data.add(new Criterion(Constants.BUILDING_ROOFINGMATERIAL, building.getRoofingMaterial()));
        data.add(new Criterion(Constants.BUILDING_FLOORSPACE, building.getFloorSpace()));
        data.add(new Criterion(Constants.BUILDING_EXTERIORBUILDINGSIZE, building.getExteriorBuildingSize()));
        
        return id;
    }
    
}
