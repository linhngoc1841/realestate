/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Domain.BaseType;
import Utils.Constants;
import Utils.Criterion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author linh
 */
public class BaseTypeDao extends BaseDao<BaseType>{
    public BaseTypeDao(String type){
        tableName = type;
    }
    @SuppressWarnings("unchecked")
    public void convertToDomain(ResultSet data, Vector<BaseType> baseTypes)
    {
        try {
            while(data.next())
            {
                BaseType baseType =new BaseType();
                baseType.setId(data.getInt(Constants.ID));
                baseType.setName(data.getString(Constants.Name));
                baseTypes.add(baseType);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseTypeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    public Vector convertToTbData(ResultSet data, int colCount)
    {
        Vector baseTypes = new Vector<>();
        try {
            while(data.next())
            {
                Vector record = new Vector();
                for (int i = 0; i < colCount; i++) {
                    record.add(data.getString(i + 1));
                }
                baseTypes.add(record);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseTypeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return baseTypes;
    }
    
    @SuppressWarnings("unchecked")
    public Object convertToData(BaseType baseType,Vector<Criterion> data)
    {
        Object id =baseType.getId();
        data.add(new Criterion(Constants.Name, baseType.getName()));
        return id;
    }
}
