/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package realestate;

import Domain.BaseType;
import Service.PropertyService;
import Utils.CommonMethod;
import Utils.Condition;
import Utils.Constants;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author linh
 */
public class SearchPropertyJFrame extends javax.swing.JFrame {

    /**
     * Creates new form DispalyPropertyJFrame
     */
    public SearchPropertyJFrame() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        propertiesSrvice.getConnection();
        propertyTypes = CommonMethod.addDataIntoCombobox(cmbPropertyType,Constants.PT_TB);
        buildingTypes = CommonMethod.addDataIntoCombobox(cmbBuildingType,Constants.BT_TB);
        farmTypes = CommonMethod.addDataIntoCombobox(cmbFarmType,Constants.FT_TB);
        livestock = CommonMethod.addDataIntoCombobox(cmbLiveStock,Constants.LS_TB);
        crop = CommonMethod.addDataIntoCombobox(cmbCrop,Constants.CROP_TB);
        double price = 25000;
        for(int i=0; i<40;i++){
            cmbLowerPrice.addItem(price);
            cmbUpperPrice.addItem(price);
            price += 25000;
        }
    }
    
    private void search(){
        Condition condition = new Condition();
        if(cmbPropertyType.getSelectedIndex() > 0)
            condition.setPropertyTypeId(propertyTypes.get(cmbPropertyType.getSelectedIndex()-1).getId());
        else
            condition.setPropertyTypeId(0);
        if(cmbBuildingType.getSelectedIndex() > 0)
            condition.setBuildingTypeId(buildingTypes.get(cmbBuildingType.getSelectedIndex()-1).getId());
        else
            condition.setBuildingTypeId(0);
        if(cmbCrop.getSelectedIndex() > 0)
            condition.setCropId(crop.get(cmbCrop.getSelectedIndex()-1).getId());
        else
            condition.setCropId(0);
        if(cmbFarmType.getSelectedIndex() > 0)
            condition.setFarmTypeId(farmTypes.get(cmbFarmType.getSelectedIndex()-1).getId());
        else
            condition.setFarmTypeId(0);
        if(cmbLiveStock.getSelectedIndex() > 0)
            condition.setLiveStockId(livestock.get(cmbLiveStock.getSelectedIndex()-1).getId());
        else
            condition.setLiveStockId(0);
        if(cmbLowerPrice.getSelectedIndex() > 0)
            condition.setLowerPrice(Double.parseDouble(cmbLowerPrice.getSelectedItem().toString()));
        else
            condition.setLowerPrice(0);
        if(cmbUpperPrice.getSelectedIndex() > 0)
            condition.setUpperPrice(Double.parseDouble(cmbUpperPrice.getSelectedItem().toString()));
        else
            condition.setUpperPrice(0);
        if(cmbSaleRent.getSelectedIndex() > 0)
            condition.setSaleRent(cmbSaleRent.getSelectedIndex()-1);
        else
            condition.setSaleRent(-1);
        Vector colNames = new Vector();
        Vector data = propertiesSrvice.searchProperties(condition, colNames);
        tbModel.setDataVector(data,colNames);
        tbProberties.setModel(tbModel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbPropertyType = new javax.swing.JComboBox();
        cmbSaleRent = new javax.swing.JComboBox();
        cmbLowerPrice = new javax.swing.JComboBox();
        cmbUpperPrice = new javax.swing.JComboBox();
        cmbBuildingType = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbProberties = new javax.swing.JTable();
        btnSearch = new javax.swing.JButton();
        cmbCrop = new javax.swing.JComboBox();
        cmbLiveStock = new javax.swing.JComboBox();
        cmbFarmType = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        cmbPropertyType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Property Type" }));
        cmbPropertyType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbPropertyTypeItemStateChanged(evt);
            }
        });

        cmbSaleRent.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sale And Rent", "Rent", "Sale" }));

        cmbLowerPrice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Price Limit" }));

        cmbUpperPrice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Upper Price Limit" }));

        cmbBuildingType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Building Type" }));

        tbProberties.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbProberties.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbProbertiesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbProberties);

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        cmbCrop.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Crop" }));

        cmbLiveStock.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Live Stock" }));

        cmbFarmType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FarmType" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbFarmType, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(cmbLiveStock, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(cmbCrop, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbPropertyType, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(cmbSaleRent, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(cmbLowerPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(cmbUpperPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(cmbBuildingType, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbPropertyType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbSaleRent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbLowerPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbUpperPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbBuildingType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFarmType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbLiveStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbCrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbProbertiesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbProbertiesMouseClicked
        int row = tbProberties.getSelectedRow();
        Object id = tbProberties.getModel().getValueAt(row, 0);
        DisplayPropertyJFrame displayPropertyJFrame = new DisplayPropertyJFrame(id);
        displayPropertyJFrame.setVisible(true);
    }//GEN-LAST:event_tbProbertiesMouseClicked

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        search();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        propertiesSrvice.closeConnection();
    }//GEN-LAST:event_formWindowClosed

    private void cmbPropertyTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbPropertyTypeItemStateChanged
        // TODO add your handling code here:
        if(Constants.AGRICULTURE.equals(cmbPropertyType.getSelectedItem())){
            cmbFarmType.setEnabled(true);
            cmbLiveStock.setEnabled(true);
            cmbCrop.setEnabled(true);
        }else{
            cmbFarmType.setSelectedIndex(0);
            cmbLiveStock.setSelectedIndex(0);
            cmbCrop.setSelectedIndex(0);
            cmbFarmType.setEnabled(false);
            cmbLiveStock.setEnabled(false);
            cmbCrop.setEnabled(false);
        }
    }//GEN-LAST:event_cmbPropertyTypeItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SearchPropertyJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SearchPropertyJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SearchPropertyJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SearchPropertyJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SearchPropertyJFrame().setVisible(true);
            }
        });
    }
    
    Vector<BaseType> propertyTypes;
    Vector<BaseType> buildingTypes;
    Vector<BaseType> farmTypes;
    Vector<BaseType> livestock;
    Vector<BaseType> crop;
    PropertyService propertiesSrvice = new PropertyService();
    DefaultTableModel tbModel = new DefaultTableModel();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cmbBuildingType;
    private javax.swing.JComboBox cmbCrop;
    private javax.swing.JComboBox cmbFarmType;
    private javax.swing.JComboBox cmbLiveStock;
    private javax.swing.JComboBox cmbLowerPrice;
    private javax.swing.JComboBox cmbPropertyType;
    private javax.swing.JComboBox cmbSaleRent;
    private javax.swing.JComboBox cmbUpperPrice;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbProberties;
    // End of variables declaration//GEN-END:variables
}
