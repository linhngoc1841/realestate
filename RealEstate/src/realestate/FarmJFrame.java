/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package realestate;

import Domain.BaseType;
import Domain.FarmDes;
import Service.FarmDesService;
import Utils.CommonMethod;
import Utils.Constants;
import Utils.Criterion;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author linh
 */
public class FarmJFrame extends javax.swing.JFrame {

    /**
     * Creates new form FarmJFrame
     */
    public FarmJFrame(ProperTiesJFrame properTiesJFrame) {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        farmDesService.getConnection();
        this.properTiesJFrame = properTiesJFrame;
        load();
        farmtypes = CommonMethod.addDataIntoCombobox(cmbFarmType,Constants.FT_TB);
        crops = CommonMethod.addDataIntoCombobox(cmbCrop, Constants.CROP_TB);
        livestocks = CommonMethod.addDataIntoCombobox(cmbLiveStock, Constants.LS_TB);
        btnDelete.setEnabled(false);
        btnSave.setEnabled(false);
    }
    
    private void load(){
        Vector colNames = new Vector();
        Vector<Criterion> criterions = new Vector<>();
        criterions.add(new Criterion(Constants.FD_PROPERTYID, properTiesJFrame.getProperty().getId()+""));
        Vector data = farmDesService.getBy(colNames,criterions,null);
        tbModel.setDataVector(data,colNames);
        tbFarmDes.setModel(tbModel);
    }
    
    private void clear(){
        txtId.setText("");
        cmbFarmType.setSelectedIndex(0);
        cmbCrop.setSelectedIndex(0);
        cmbLiveStock.setSelectedIndex(0);
        btnDelete.setEnabled(false);
        btnSave.setEnabled(false);
        btnAdd.setEnabled(true);
    }
    
    private void setData(){
        farmDes.setProperties(properTiesJFrame.getProperty());
        farmDes.setFarmType(farmtypes.get(cmbFarmType.getSelectedIndex()));
        if(cmbCrop.getSelectedIndex()>0)
            farmDes.setCrop(crops.get(cmbCrop.getSelectedIndex()-1));
        else
            farmDes.setCrop(null);
        if(cmbLiveStock.getSelectedIndex()>0)
            farmDes.setLiveStock(livestocks.get(cmbLiveStock.getSelectedIndex()-1));    
        else
            farmDes.setLiveStock(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbFarmType = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cmbCrop = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cmbLiveStock = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbFarmDes = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setText("Farm Type");

        jLabel2.setText("Crop");

        cmbCrop.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "null" }));

        jLabel3.setText("LiveStock");

        cmbLiveStock.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "null" }));

        jLabel14.setText("Id");

        txtId.setEnabled(false);

        tbFarmDes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbFarmDesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbFarmDes);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel14)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cmbCrop, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbFarmType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbLiveStock, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(202, 202, 202)
                        .addComponent(btnClear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete))
                    .addComponent(jScrollPane1))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFarmType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbLiveStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear)
                    .addComponent(btnAdd)
                    .addComponent(btnSave)
                    .addComponent(btnDelete))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbFarmDesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbFarmDesMouseClicked
        // TODO add your handling code here:
        int row = tbFarmDes.getSelectedRow();
        Object id = tbFarmDes.getModel().getValueAt(row, 0);
        farmDes = farmDesService.getById(id);
        txtId.setText(farmDes.getId()+"");
        cmbFarmType.setSelectedItem(farmDes.getFarmType().getName());
        if(farmDes.getCrop() != null)
            cmbCrop.setSelectedItem(farmDes.getCrop().getName());
        else
            cmbCrop.setSelectedIndex(0);
        if(farmDes.getLiveStock() != null)
            cmbLiveStock.setSelectedItem(farmDes.getLiveStock().getName());
        else
            cmbLiveStock.setSelectedIndex(0);
        btnAdd.setEnabled(false);
        btnDelete.setEnabled(true);
        btnSave.setEnabled(true);
    }//GEN-LAST:event_tbFarmDesMouseClicked

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        setData();
        if(farmDesService.updatedObject(farmDes)>0){
            JOptionPane.showMessageDialog(null, "Update Successfully","Info", JOptionPane.INFORMATION_MESSAGE);
            load();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        if(farmDesService.deleteById(farmDes.getId())>0){
            load();
            clear();
            JOptionPane.showMessageDialog(null, "Delete Successfully","Info", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
       clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        farmDes = new FarmDes();
        setData();
        if(farmDesService.insertObject(farmDes)>0){
            JOptionPane.showMessageDialog(null, "Add Successfully","Info", JOptionPane.INFORMATION_MESSAGE);
            load();
            clear();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        farmDesService.closeConnection();
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FarmJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FarmJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FarmJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FarmJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FarmJFrame(null).setVisible(true);
            }
        });
    }
    
    FarmDes farmDes = new FarmDes();
    FarmDesService farmDesService = new FarmDesService();
    DefaultTableModel tbModel = new DefaultTableModel();
    ProperTiesJFrame properTiesJFrame;
    Vector<BaseType> farmtypes;
    Vector<BaseType> crops;
    Vector<BaseType> livestocks;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cmbCrop;
    private javax.swing.JComboBox cmbFarmType;
    private javax.swing.JComboBox cmbLiveStock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbFarmDes;
    private javax.swing.JTextField txtId;
    // End of variables declaration//GEN-END:variables
}
