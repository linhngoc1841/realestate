/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author linh
 */
public class BuildingDes extends BaseDomain{
    Building building;
    BaseType room;
    BaseType floor;
    String demesion;
    int count;

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public BaseType getRoom() {
        return room;
    }

    public void setRoom(BaseType room) {
        this.room = room;
    }

    public BaseType getFloor() {
        return floor;
    }

    public void setFloor(BaseType floor) {
        this.floor = floor;
    }

    public String getDemesion() {
        return demesion;
    }

    public void setDemesion(String demesion) {
        this.demesion = demesion;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
}
