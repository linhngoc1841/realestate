/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author linh
 */

public class Land extends BaseDomain{
    String soilType;
    String surfaceWater;
    String fencing;

    public String getSoilType() {
        return soilType;
    }

    public void setSoilType(String soilType) {
        this.soilType = soilType;
    }

    public String getSurfaceWater() {
        return surfaceWater;
    }

    public void setSurfaceWater(String surfaceWater) {
        this.surfaceWater = surfaceWater;
    }

    public String getFencing() {
        return fencing;
    }

    public void setFencing(String fencing) {
        this.fencing = fencing;
    }
    
}
