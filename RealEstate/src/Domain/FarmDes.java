/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author linh
 */
public class FarmDes extends BaseDomain{
    Properties properties;
    BaseType farmType;
    BaseType liveStock;
    BaseType crop;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    
    public BaseType getFarmType() {
        return farmType;
    }

    public void setFarmType(BaseType farmType) {
        this.farmType = farmType;
    }

    public BaseType getLiveStock() {
        return liveStock;
    }

    public void setLiveStock(BaseType liveStock) {
        this.liveStock = liveStock;
    }

    public BaseType getCrop() {
        return crop;
    }

    public void setCrop(BaseType crop) {
        this.crop = crop;
    }
    
}
