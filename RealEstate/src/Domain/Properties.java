/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.List;

/**
 *
 * @author linh
 */

public class Properties extends BaseDomain{
    BaseType propertyType;
    Building building;
    Land land;
    int saleOrrent;
    String title;
    String address;
    String landSize;
    double Price;
    String BuiltIn;
    int storeys;
    String parkingType;
    String scene;
    String features;
    String appliancesIncluded;
    String amenitiesNearby;
    String description;
    List<FarmDes> farmDeses;

    public BaseType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(BaseType propertyType) {
        this.propertyType = propertyType;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    public int getSaleOrrent() {
        return saleOrrent;
    }

    public void setSaleOrrent(int saleOrrent) {
        this.saleOrrent = saleOrrent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandSize() {
        return landSize;
    }

    public void setLandSize(String landSize) {
        this.landSize = landSize;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public String getBuiltIn() {
        return BuiltIn;
    }

    public void setBuiltIn(String BuiltIn) {
        this.BuiltIn = BuiltIn;
    }

    public int getStoreys() {
        return storeys;
    }

    public void setStoreys(int storeys) {
        this.storeys = storeys;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getAppliancesIncluded() {
        return appliancesIncluded;
    }

    public void setAppliancesIncluded(String appliancesIncluded) {
        this.appliancesIncluded = appliancesIncluded;
    }

    public String getAmenitiesNearby() {
        return amenitiesNearby;
    }

    public void setAmenitiesNearby(String amenitiesNearby) {
        this.amenitiesNearby = amenitiesNearby;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public List<FarmDes> getFarmDeses() {
        return farmDeses;
    }

    public void setFarmDeses(List<FarmDes> farmDeses) {
        this.farmDeses = farmDeses;
    }

}
