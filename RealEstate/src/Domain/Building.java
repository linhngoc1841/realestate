/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.List;

/**
 *
 * @author linh
 */
public class Building extends BaseDomain{
    String heatingType;
    String heatingFuel;
    String flooring;
    String foundationType;
    String roofStyle;
    String roofingMaterial;
    String floorSpace;
    String exteriorBuildingSize;
    BaseType buildingType;
    List<BuildingDes> buildingDeses;
    
    public String getHeatingType() {
        return heatingType;
    }

    public void setHeatingType(String heatingType) {
        this.heatingType = heatingType;
    }

    public String getHeatingFuel() {
        return heatingFuel;
    }

    public void setHeatingFuel(String heatingFuel) {
        this.heatingFuel = heatingFuel;
    }

    public String getFlooring() {
        return flooring;
    }

    public void setFlooring(String flooring) {
        this.flooring = flooring;
    }

    public String getFoundationType() {
        return foundationType;
    }

    public void setFoundationType(String foundationType) {
        this.foundationType = foundationType;
    }

    public String getRoofStyle() {
        return roofStyle;
    }

    public void setRoofStyle(String roofStyle) {
        this.roofStyle = roofStyle;
    }

    public String getRoofingMaterial() {
        return roofingMaterial;
    }

    public void setRoofingMaterial(String roofingMaterial) {
        this.roofingMaterial = roofingMaterial;
    }

    public String getFloorSpace() {
        return floorSpace;
    }

    public void setFloorSpace(String floorSpace) {
        this.floorSpace = floorSpace;
    }

    public String getExteriorBuildingSize() {
        return exteriorBuildingSize;
    }

    public void setExteriorBuildingSize(String exteriorBuildingSize) {
        this.exteriorBuildingSize = exteriorBuildingSize;
    }

    public BaseType getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(BaseType buildingType) {
        this.buildingType = buildingType;
    }
    
    public List<BuildingDes> getBuildingDeses() {
        return buildingDeses;
    }

    public void setBuildingDeses(List<BuildingDes> buildingDeses) {
        this.buildingDeses = buildingDeses;
    }
}
