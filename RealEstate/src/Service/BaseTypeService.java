/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.BaseTypeDao;
import Domain.BaseType;

/**
 *
 * @author linh
 */
public class BaseTypeService extends BaseService<BaseType> {
    private BaseTypeDao baseTypeDao;

    public BaseTypeService(String type) {
        baseTypeDao = new BaseTypeDao(type);
    }
    	
    public BaseDao<BaseType> getDao(){
        return baseTypeDao;
    }
}
