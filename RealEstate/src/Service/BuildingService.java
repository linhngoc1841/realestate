/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.BuildingDao;
import Domain.Building;

/**
 *
 * @author linh
 */
public class BuildingService extends BaseService<Building> {
    private BuildingDao buildingDao;

    public BuildingService() {
        buildingDao = new BuildingDao();
    }
    	
    public BaseDao<Building> getDao(){
        return buildingDao;
    }
}
