/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.FarmDesDao;
import Domain.FarmDes;

/**
 *
 * @author linh
 */

public class FarmDesService extends BaseService<FarmDes> {
    private FarmDesDao farmDao;

    public FarmDesService() {
        farmDao = new FarmDesDao();
    }
    	
    public BaseDao<FarmDes> getDao(){
        return farmDao;
    }
    
}