/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.PropertyDao;
import Domain.FarmDes;
import Domain.Properties;
import Utils.Condition;
import Utils.Constants;
import Utils.Criterion;
import java.util.Vector;

/**
 *
 * @author linh
 */
public class PropertyService extends BaseService<Properties> {
    private PropertyDao propertyDao;

    public PropertyService() {
        propertyDao = new PropertyDao();
    }
    	
    public BaseDao<Properties> getDao(){
        return propertyDao;
    }
    
    public Vector searchProperties(Condition condition, Vector colNames)
    {   Vector searchData = new Vector();
        FarmDesService farmDesService = new FarmDesService();
        farmDesService.getConnection();
        
        Vector<Properties> properties = propertyDao.getAll();
        Vector data = getAll(colNames);
        
        for(int i=0;i<properties.size();i++){
            
            
            Properties property = properties.get(i);
            if(condition.getPropertyTypeId() > 0 && property.getPropertyType().getId() != condition.getPropertyTypeId()){
                continue;
            }
            if(condition.getSaleRent() > -1 && property.getSaleOrrent() != condition.getSaleRent()){
                continue;
            }
            if(condition.getLowerPrice() > 0 && property.getPrice() < condition.getLowerPrice()){
                continue;
            }
            if(condition.getUpperPrice() > 0 && property.getPrice() > condition.getUpperPrice()){
                continue;
            }
            if(condition.getBuildingTypeId() > 0 ){
                if(property.getBuilding()==null){
                    continue;
                }else{
                    if(property.getBuilding().getBuildingType().getId() != condition.getBuildingTypeId()){
                        continue;
                    }
                }
            }
            Vector<Criterion> criterions = new Vector<>();
            criterions.add(new Criterion(Constants.FD_PROPERTYID, property.getId()+""));
            Vector<FarmDes> farmDeses = farmDesService.getBy(criterions);
            if(condition.getFarmTypeId() > 0){
                int j=0;
                for(j=0;j<farmDeses.size();j++){
                    if(farmDeses.get(j).getFarmType().getId() == condition.getFarmTypeId())
                        break;
                }
                if(j==farmDeses.size()){
                    continue;
                }
            }
            if(condition.getLiveStockId() > 0 ){
                int j=0;
                for(j=0;j<farmDeses.size();j++){
                    if(farmDeses.get(j).getLiveStock()!=null)
                        if(farmDeses.get(j).getLiveStock().getId() == condition.getLiveStockId())
                            break;
                }
                if(j==farmDeses.size()){
                    continue;
                }
            }
            
            if(condition.getCropId() > 0){
                int j=0;
                for(j=0;j<farmDeses.size();j++){
                    if(farmDeses.get(j).getCrop()!=null)
                        if(farmDeses.get(j).getCrop().getId() == condition.getCropId())
                            break;
                }
                if(j==farmDeses.size()){
                    continue;
                }
            }
            searchData.add(data.get(i));
        }   
        
        farmDesService.closeConnection();
        return searchData;
        
    }
}
