/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.BuildingDesDao;
import Domain.BuildingDes;

/**
 *
 * @author linh
 */
public class BuildingDesService extends BaseService<BuildingDes> {
    private BuildingDesDao buildingDesDao;

    public BuildingDesService() {
        buildingDesDao = new BuildingDesDao();
    }
    	
    public BaseDao<BuildingDes> getDao(){
        return buildingDesDao;
    }
    
}
