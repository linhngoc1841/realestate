/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Service;

import Dao.BaseDao;
import Dao.LandDao;
import Domain.Land;
import Utils.Constants;
import Utils.Criterion;
import java.util.Vector;

/**
 *
 * @author linh
 */
public class LandService extends BaseService<Land> {
    private LandDao landDao;

    public LandService() {
        landDao = new LandDao();
    }
    	
    public BaseDao<Land> getDao(){
        return landDao;
    }
    
}
