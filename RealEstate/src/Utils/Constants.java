/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utils;

/**
 *
 * @author linh
 */
public class Constants {
    public static final String RESIDENTIAL="Residential";
    public static final String COMMERCIAL="Commercial";
    public static final String AGRICULTURE="Agriculture";
    public static final String ID="Id";
    public static final String Name="Name";
    //PropertyType
    public static final String PT_TB="PropertyType";   
    //BuildingType
    public static final String BT_TB="BuildingType";   
    //Room
    public static final String ROOM_TB="Room";   
    //Floor
    public static final String FLOOR_TB="Floor";   
    //FarmType
    public static final String FT_TB="FarmType";   
    //LiveStock
    public static final String LS_TB="LiveStock";   
    //Crop
    public static final String CROP_TB="Crop";
    
    //Building
    public static final String BUILDING_TB="Building";
    public static final String BUILDING_BT_ID="BT_Id";
    public static final String BUILDING_HEATINGTYPE="HeatingType";
    public static final String BUILDING_HEATINGFUEL="HeatingFuel";
    public static final String BUILDING_FLOORING="Flooring";
    public static final String BUILDING_FOUNDATIONTYPE="FoundationType";
    public static final String BUILDING_ROOFSTYLE="RoofStyle";
    public static final String BUILDING_ROOFINGMATERIAL="RoofingMaterial";
    public static final String BUILDING_FLOORSPACE="FloorSpace";
    public static final String BUILDING_EXTERIORBUILDINGSIZE="ExteriorBuildingSize";
    
    //BuildingDes
    public static final String BD_TB="BuildingDes";
    public static final String BD_BUILDINGID="BuildingId";
    public static final String BD_ROOMID="RoomId";
    public static final String BD_FLOORID="FloorId";
    public static final String BD_DEMESION="Demension";
    public static final String BD_COUNT="count";
    
    
    //Properties
    public static final String PRO_TB="Properties";
    public static final String PRO_PT_ID="PT_Id";
    public static final String PRO_BUILDINGID="BuildingId";
    public static final String PRO_LANDID="LandId";
    public static final String PRO_SALERENT="sale_rent";
    public static final String PRO_TITLE="title";
    public static final String PRO_ADDRESS="Address";
    public static final String PRO_LANDSIZE="LandSize";
    public static final String PRO_PRICE="Price";
    public static final String PRO_BUILT_IN="Built_in";
    public static final String PRO_STOREYS="Storeys";
    public static final String PRO_PARKINGTYPE="ParkingType";
    public static final String PRO_SCENE="scene";
    public static final String PRO_FEATURES="features";
    public static final String PRO_APP_INCLUDED="AppliancesIncluded";
    public static final String PRO_AMENITIES_NEARBY="AmenitiesNearby";
    public static final String PRO_DES="Description";

    //Land
    public static final String LAND_TB="Land";
    public static final String LAND_SOILTYPE="SoilType";
    public static final String LAND_SURFACEWATER="SurfaceWater";
    public static final String LAND_FENCING="Fencing";
    
    //FarmDes
    public static final String FD_TB="FarmDes";
    public static final String FD_PROPERTYID="PropertyId";
    public static final String FD_FRAMTYPEID="FarmTypeId";
    public static final String FD_LIVESTOCKID="LiveStockId";
    public static final String FD_CROPID="CropId";
    
}
