/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utils;

/**
 *
 * @author linh
 */
public class Condition {
    private int propertyTypeId;
    private int saleRent;
    private double lowerPrice;
    private double upperPrice;
    private int buildingTypeId;
    private int farmTypeId;
    private int liveStockId;
    private int cropId;

    public int getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(int propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    public int getSaleRent() {
        return saleRent;
    }

    public void setSaleRent(int saleRent) {
        this.saleRent = saleRent;
    }

    public double getLowerPrice() {
        return lowerPrice;
    }

    public void setLowerPrice(double lowerPrice) {
        this.lowerPrice = lowerPrice;
    }

    public double getUpperPrice() {
        return upperPrice;
    }

    public void setUpperPrice(double upperPrice) {
        this.upperPrice = upperPrice;
    }

    public int getBuildingTypeId() {
        return buildingTypeId;
    }

    public void setBuildingTypeId(int buildingTypeId) {
        this.buildingTypeId = buildingTypeId;
    }

    public int getFarmTypeId() {
        return farmTypeId;
    }

    public void setFarmTypeId(int farmTypeId) {
        this.farmTypeId = farmTypeId;
    }

    public int getLiveStockId() {
        return liveStockId;
    }

    public void setLiveStockId(int liveStockId) {
        this.liveStockId = liveStockId;
    }

    public int getCropId() {
        return cropId;
    }

    public void setCropId(int cropId) {
        this.cropId = cropId;
    }
    
}
