/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utils;

import Domain.BaseType;
import Service.BaseTypeService;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author linh
 */
public class CommonMethod {
    public static boolean checkEmptyString(String text,String errorMsg,String caption)
    {
        if ("".equals(text.trim()))
        {
            if (errorMsg != null)
                JOptionPane.showMessageDialog(null, errorMsg,caption, JOptionPane.ERROR_MESSAGE);
            return true;
        }
        return false;
    }
    
    public static boolean checkNotInt(String text, String errorMsg, String caption)
    {
        try{
            if(!"".equals(text.trim()))
                Integer.parseInt(text);
            return false;            
        }catch(Exception ex){
            if (errorMsg != null)
                JOptionPane.showMessageDialog(null, errorMsg,caption, JOptionPane.ERROR_MESSAGE);
            return true;
        }
    }
    
    public static boolean checkNotDouble(String text, String errorMsg, String caption)
    {
        try{
            if(!"".equals(text.trim()))
                Double.parseDouble(text);
            return false;            
        }catch(Exception ex){
            if (errorMsg != null)
                JOptionPane.showMessageDialog(null, errorMsg,caption, JOptionPane.ERROR_MESSAGE);
            return true;
        }
    }
    
    public static Vector<BaseType> addDataIntoCombobox(JComboBox cmb, String table){
        BaseTypeService baseTypeService = new BaseTypeService(table);
        baseTypeService.getConnection();
        Vector<BaseType> types = new Vector<>();
        types = baseTypeService.getAll();
                
        for (int i = 0; i < types.size(); i++) {
            cmb.addItem(types.get(i).getName());
        }
        baseTypeService.closeConnection();
        return types;
    }
}
